import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'

export function ShowPrefix(subnet: String, description: String){
    return(
        <>
            <br></br><br></br><a>
            <code className={styles.code}>{subnet}</code> {description}
        </a>
        </>
        )
}

export default function Home() {
    return (
        <div className={styles.container}>
            <Head>
                <title>AS212539 - OTTER-AS</title>
                <script defer data-domain="as212539.net" src="https://plausible.io/js/script.js"></script>
                <meta name="description" content="Landing page for AS212539"/>
            </Head>

            <main className={styles.main}>
                <h1 className={styles.title}>
                    <a href="https://as212539.net">AS212539</a> Prefix Announcements
                </h1>
                <p className={styles.description}>
                    This page lists all prefixes announced by this AS
                    {ShowPrefix("2a0e:97c0:3f0::/48", "Hyperbit → Office Network")}
                    {ShowPrefix("2a0e:97c0:3f1::/48", "Hyperbit → Home Network")}
                    {ShowPrefix("2a0e:97c0:3f2::/48", "Hyperbit → Reserved for client usage")}
                    {ShowPrefix("2a0e:97c0:3f3::/48", "Hyperbit → Reserved for client usage")}
                    {ShowPrefix("2a0e:97c0:3f4::/48", "Hyperbit → Reserved")}
                    {ShowPrefix("2a0e:97c0:3f5::/48", "Hyperbit → Reserved")}
                    {ShowPrefix("2a0e:97c0:3f6::/48", "OrangeFox → Network 0 → Subnet 0")}
                    {ShowPrefix("2a0e:97c0:3f7::/48", "Hyperbit → Reserved")}
                </p>
            </main>

            <footer className={styles.footer}>
                <a href="https://francescomasala.me" target="_blank" rel="noopener noreferrer">
                    Made and maintained with ❤️ by Francesco Masala
                </a>
                <br></br>
                <a href="https://gitlab.com/AS212539/Website/" target="_blank" rel="noopener noreferrer">
                    GitLab
                </a>
            </footer>
        </div>
    )
}
