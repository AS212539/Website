import Head from 'next/head'
import Image from 'next/image'
import styles from '../styles/Home.module.css'
import Link from "next/link";

function Card(title: string, description: string, link: string) {
    return (
        <Link href={link}>
          <div className={styles.card}>
            <h2>{title} &rarr;</h2>
            <p>{description}</p>
          </div>
        </Link>
    )
}

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>AS212539 - OTTER-AS</title>
        <script defer data-domain="as212539.net" src="https://plausible.io/js/script.js"></script>
        <meta name="description" content="Landing page for AS212539" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to the landing page of <a>AS212539</a>
        </h1>

        <p className={styles.description}>
          Yeah, I know I should have a better landing page, but I&apos;m lazy.
        </p>

        <div className={styles.grid}>
            {Card("BGP Info", "Check out the page for this AS on the Hurricane Electric portal", "https://bgp.he.net/AS212539")}
            {Card("Contacts", "Contact me if you want information about peering or to report a problem", "mailto:fmasala@hyperbit.it")}
            {Card("Grafana", "You can view a graphing dashboard that shows all the various upstreams, peers, and prefix visibility", "https://grafana.hyperbit.it/d/W50XH6dVz/as212539")}
            {Card("Prefixes", "This page lists all prefixes announced by this AS", "/prefixes")}
        </div>
      </main>

      <footer className={styles.footer}>
        <a href="https://francescomasala.me" target="_blank" rel="noopener noreferrer">
          Made and maintained with ❤ by Francesco Masala
        </a>
        <br></br>
        <a href="https://gitlab.com/AS212539/Website/" target="_blank" rel="noopener noreferrer">
            GitLab
        </a>
      </footer>
    </div>
  )
}
